
## About Laravel

Laravel is a web application framework with expressive, elegant syntax. Development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give tools we need to build any application with which we are tasked.

## About Application

This repository contains the version of Laravel 5.5. This is a simple coding task that only has Create and Read of the small CRUD part for the client section of the application. The Client-form has name, gender, phone, email, address, nationality, date of birth, education background, and preferred mode of contact fields along with validation rules using Laravel Request. 

The Client data is saved to a CSV file placed in /storage directory after the form submission. In order to track log-files to maintain client records, this application utilizes Monolog Library. With this log-record maintained, all the clients created/imported can be listed and is sorted to descending order. Manual Pagination is being implemented in the clients list.

There is an option to import and export client records to CSV.

## Laravel Packages used
## - LaravelCollective/html
LaravelCollective's HTML package is ease of use. It reduces code writing. Form facade makes select fields much more painless when we're trying to pre select an option. It beats writing lots of loops and if statements in our blade syntax. Using the Form::open method with POST, PUT or DELETE the CSRF token will be added to our forms as a hidden field automatically. CSRF protects our application from cross-site request forgeries.

## - Maatwebsite/Laravel-Excel
Laravel Excel brings the power of PHPOffice's PHPExcel to Laravel 5 with a touch of the Laravel Magic. It includes features like: importing Excel and CSV to collections, exporting models, array's and views to Excel, importing batches of files and importing a file by a config file.

- Import into Laravel Collections
- Export Blade views to Excel and CSV with optional CSS styling
- Batch imports
- A lot of optional config settings
- Easy cell caching
- Chunked and queued importer
- ExcelFile method injections
- Editing existing Excel files
- Advanced import by config files
- and many more...

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).