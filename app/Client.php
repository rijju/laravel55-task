<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $dates = ['dob'];
    protected $fillable = ['name','gender','phone','email','address','nationality','dob','edu_bg','pr_moc'];
}
