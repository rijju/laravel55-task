<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Client Lists';
        $clientLogArray = $this->getClientLogs();

        $perPage = 5;
        $clientsData = $this->pagination($clientLogArray,$perPage);

        return view('client.index')
            ->with('title',$title)
            ->with('clients',$clientsData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Add New Client';
        $gender = array_map('ucfirst',['male'=>'male','female'=>'female','other'=>'other']);
        $preferredMoc = array_map('ucfirst',['none'=>'none','phone'=>'phone','email'=>'email']);

        return view('client.create')
            ->with([
                'title' => $title,
                'gender' => $gender,
                'preferredMoc'=> $preferredMoc
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $clientArray[] = $data;

        $filename = 'client_'.time();
        $format = 'csv';

        Log::info('Client Created',$data);
        Excel::create($filename, function($excel) use ($clientArray) {
            $excel->sheet('Sheet1', function($sheet) use ($clientArray)
            {
                $sheet->fromArray($clientArray);
            });
        })->store($format,storage_path('/app/public/client_csv'));

        return redirect('client')->withSuccess('Client Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($client)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    /*
     * Export Client Data
     *
     * @param export-format
     */
    public function downloadClients($format)
    {
        $clientArray = $this->getClientLogs();
        $filename = 'client_'.time();
        Excel::create($filename, function($excel) use ($clientArray) {
            $excel->sheet('Sheet1', function($sheet) use ($clientArray)
            {
                $sheet->fromArray($clientArray);
            });
        })->download($format);
    }

    /*
     * Import Client Data
     *
     * @param \Illuminate\Http\Request  $request
     * @param import-format
     */
    public function importClients(Request $request, $format)
    {
        if ($request->hasFile('import_clients')) {
            $path = $request->file('import_clients')->getRealPath();
            $clientArray = Excel::load($path, function ($reader) {
                foreach ($reader->toArray() as $key => $row) {
                    $keys = ['name','gender','phone','email','address','nationality','dob','education_background','preferred_moc'];
                    if($this->array_keys_exists($keys,$row)) Log::info('Client Imported',$row);
                    else return redirect()->back()->withErrors('Something went wrong. Please try again');
                }
            })->toArray();

            $filename = 'client_imported_'.time();
            Excel::create($filename, function($excel) use ($clientArray) {
                $excel->sheet('Sheet1', function($sheet) use ($clientArray)
                {
                    $sheet->fromArray($clientArray);
                });
            })->store($format,storage_path('/app/public/client_csv'));

            $filenameWithExt = $filename.'.'.$format;
            return redirect('client/import/'.$filenameWithExt)->withSuccess('Clients Imported Successfully');
        }else return redirect()->back()->withErrors('Something went wrong. Please try again');
    }

    function array_keys_exists(array $keys, array $arr) {
        return !array_diff_key(array_flip($keys), $arr);
    }

    /*
     * Get imported clients
     *
     * @param $filename
     * @return \Illuminate\Http\Response
     */
    public function getImportedClients($filename){
        $title = 'Imported Clients';

        $file = public_path('storage/client_csv/'.$filename);
        $clientArray = $this->csvToArray($file);

        return view('client.import.list')
            ->with('title',$title)
            ->with('clients',$clientArray);
    }

    /*
     *  Get Client Logs
     *
     * return array $clientLogArray
     */
    public function getClientLogs(){
        $clientLogs = file(public_path().'/storage/client_logs/clients.log');
        arsort($clientLogs);

        $pattern = '/\{(?:[^{}]|(?R))*\}/';
        $clientLogArray = [];

        foreach ($clientLogs as $line_num => $client) {
            preg_match($pattern, $client, $matches);
            if($matches) $clientLogArray[$line_num] = json_decode($matches[0],TRUE);
        }

        return $clientLogArray;
    }

    /*
     * enum values from database-table
     *
     * @param table,column name
     * @return array $enum
     */
    public static function getEnumValues($table, $column) {
        $type = DB ::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }

    /*
     * Convert CSV to Array
     *
     * @param $filename, $delimeter
     * @return array $data
     */
    function csvToArray($filename = '', $delimiter = ','){
        if (!file_exists($filename) || !is_readable($filename)) return false;

        $header = null;
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) $header = $row;
                else $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /*
     * Pagination
     *
     * @param array $clientLogArray
     * @param $perPage
     * @return array $clientsData
     */
    public function pagination($clientLogArray,$perPage){
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $clientLogCollection = new Collection($clientLogArray);

        $currentPageSearchResults = $clientLogCollection->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $clientsData = new LengthAwarePaginator($currentPageSearchResults, count($clientLogCollection), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));

        return $clientsData;
    }
}
