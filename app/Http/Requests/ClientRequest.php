<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'gender' => 'required',
            'phone' => 'sometimes|nullable|digits_between:7,10',
            'email' => 'required|email'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Please specify name',
            'gender.required'  => 'Please specify gender',
            'phone.integer' => 'Please specify valid phone number',
            'phone.digits_between' => 'Please specify valid phone number',
            'email.required' => 'Please specify email',
        ];
    }
}
