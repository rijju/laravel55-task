<?php

use App\Client;
use App\Http\Controllers\ClientController;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::truncate();

        $faker = Faker::create();
        $gender = array_map('ucfirst',ClientController::getEnumValues('clients','gender'));
        $pr_moc = array_map('ucfirst',ClientController::getEnumValues('clients','pr_moc'));

        $clientArray=[];
        foreach(range(1,10) as $index){
            $client=[];
            $client['name'] = $faker->name;
            $client['gender'] = $faker->randomElement($gender);
            $client['phone'] = mt_rand(1000000000, mt_getrandmax());
            $client['email'] = $faker->unique()->email;
            $client['address'] = $faker->address;
            $client['nationality'] = str_random(10);
            $client['dob'] = $faker->dateTimeBetween('1990-01-01','2012-12-31')->format('Y-m-d');
            $client['edu_bg'] = str_random(30);
            $client['pr_moc'] = $faker->randomElement($pr_moc);
            $client['created_at'] = Carbon::now();
            $client['updated_at'] = Carbon::now();

            $clientArray[] = $client;
        }

        Client::insert($clientArray);
    }
}
