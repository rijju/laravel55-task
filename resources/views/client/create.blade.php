@extends('layouts.app')
@section('title',$title)
@section('content')
    <h4> {{$title}} </h4>
    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open(['action'=>'ClientController@store','method'=>'post','id'=>'client_form']) !!}
            <div class="row">
                <!-- Left Section -->
                <div class="col-md-6 col-sm-6">
                    <div class="form-group required {{$errors->first('name') ? 'has-error' : ''}}">
                        {!! Form::label('name','Name',['class'=>'control-label']) !!}
                        {!! Form::text('name','',['class'=>'form-control','placeholder'=>'Enter Name','minlength'=>2,'required']) !!}
                        <strong class="help-block">{{$errors->first('name')}}</strong>
                    </div>
                    <div class="form-group required {{$errors->first('gender') ? 'has-error' : ''}}">
                        {!! Form::label('gender','Gender',['class'=>'control-label']) !!}
                        <div>
                            @foreach($gender as $g)
                                <label class="radio-inline">{!! Form::radio('gender',$g,'',['required']) !!} {{$g}} </label>
                            @endforeach
                        </div>
                        <strong class="help-block">{{$errors->first('gender')}}</strong>
                    </div>
                    <div class="form-group {{$errors->first('phone') ? 'has-error' : ''}}">
                        {!! Form::label('phone','Phone',['class'=>'control-label']) !!}
                        {!! Form::text('phone','',['class'=>'form-control','placeholder'=>'Enter Phone','maxlength'=>15]) !!}
                        <strong class="help-block">{{$errors->first('phone')}}</strong>
                    </div>
                    <div class="form-group required {{$errors->first('email') ? 'has-error' : ''}}">
                        {!! Form::label('email','Email',['class'=>'control-label']) !!}
                        {!! Form::email('email','',['class'=>'form-control','placeholder'=>'Enter Email','required']) !!}
                        <strong class="help-block">{{$errors->first('email')}}</strong>
                    </div>
                    <div class="form-group">
                        {!! Form::label('address','Address',['class'=>'control-label']) !!}
                        {!! Form::text('address','',['class'=>'form-control','placeholder'=>'Enter Address']) !!}
                    </div>
                </div>
                <!-- Right Section -->
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        {!! Form::label('nationality','Nationality',['class'=>'control-label']) !!}
                        {!! Form::text('nationality','',['class'=>'form-control','placeholder'=>'Enter Nationality']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('dob','Date of birth',['class'=>'control-label']) !!}
                        <div class="input-group date" id="datepicker" data-provide="datepicker">
                            {!! Form::text('dob','',['class'=>'form-control','placeholder'=>'d/m/Y']) !!}
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('education_background','Education Background',['class'=>'control-label']) !!}
                        {!! Form::text('education_background','',['class'=>'form-control','placeholder'=>'Enter Education Background']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('preferred_moc','Preferred Mode of Contact',['class'=>'control-label']) !!}
                        {!! Form::select('preferred_moc',$preferredMoc,'none',['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            {{ Form::button('<span class="glyphicon glyphicon-save" aria-hidden="true"></span> Save', [
                'type' => 'submit',
                'class' => 'btn btn-primary']
            ) }}
            {!! Form::close() !!}
        </div>
    </div>

@endsection
