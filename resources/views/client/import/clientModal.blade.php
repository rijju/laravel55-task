<!-- Modal to import client data-->
<div class="modal fade" id="importClients" tabindex="-1" role="dialog" aria-labelledby="importClientsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-10">
                        <h5 class="modal-title" id="importClientsLabel">Import Clients</h5>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::open(['action'=>['ClientController@importClients','csv'],'method'=>'POST','files'=>true]) !!}
            <div class="modal-body">
                {!! Form::file('import_clients',['required']) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-sm btn-primary">Import</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>