@extends('layouts.app')
@section('title',$title)
@section('content')
    <h3> {{$title}} </h3>
    <a href="{{url('client')}}" class="btn btn-sm btn-success">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        View all Clients
    </a> <br /> <br />

    <table class="table responsive table-hover table-bordered table-striped no-padding">
        <thead>
        <tr>
            <th>Name</th>
            <th>Gender</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Address</th>
            <th>Nationality</th>
            <th>Date of birth</th>
            <th>Education background</th>
            <th>Preferred Mode of Contact</th>
        </tr>
        </thead>
        <tbody>
        @if(count($clients)>0)
            @foreach($clients as $line_num=>$client)
                @if($client)
                    <tr>
                        <td>{{ucfirst($client['name'])}}</td>
                        <td>{{ucfirst($client['gender'])}}</td>
                        <td>{{$client['phone'] ? $client['phone'] :'-'}}</td>
                        <td>{{$client['email'] ? $client['email'] : '-'}}</td>
                        <td>{{$client['address'] ? $client['address'] : '-'}}</td>
                        <td>{{$client['nationality'] ? $client['nationality'] : '-'}}</td>
                        <td>{{$client['dob'] ? $client['dob'] : '-'}}</td>
                        <td>{{array_key_exists('education_background',$client) && $client['education_background'] ? $client['education_background'] : '-'}}</td>
                        <td>{{array_key_exists('preferred_moc',$client) ? ucfirst($client['preferred_moc']) :''}}</td>
                    </tr>
                @endif
            @endforeach
        @else
            <tr class="danger"><td colspan="100%" class="text-center"> No Clients Imported</td></tr>
        @endif
        </tbody>
    </table>

@endsection