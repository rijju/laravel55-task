@extends('layouts.app')
@section('title',$title)
@section('content')
    <h3> {{$title}} </h3>
    <div class="row">
        <div class="col-xs-6 text-left">
            <div class="previous">
                <a href="{{url('client/create')}}" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add New Client
                </a>
            </div>
        </div>
        <div class="col-xs-6 text-right">
            <div class="next">
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#importClients">
                    <span class="glyphicon glyphicon-import" aria-hidden="true"></span>
                    Import
                </button>

                @if(count($clients)>0)
                    <a href="{{url('client/download/csv')}}" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
                        Export
                    </a>
                @endif
            </div>
        </div> <br /> <br />
    </div>
    <table class="table responsive table-hover table-bordered table-striped no-padding">
        <thead>
        <tr>
            <th>Name</th>
            <th>Gender</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Address</th>
            <th>Nationality</th>
            <th>Date of birth</th>
            <th>Education background</th>
            <th>Preferred Mode of Contact</th>
            <th width="7%">Action</th>
        </tr>
        </thead>
        <tbody>
        @if(count($clients)>0)
            @foreach($clients as $line_num=>$client)
                @if($client)
                <tr>
                    <td>{{ucfirst($client['name'])}}</td>
                    <td>{{ucfirst($client['gender'])}}</td>
                    <td>{{$client['phone'] ? $client['phone'] :'-'}}</td>
                    <td>{{$client['email'] ? $client['email'] : '-'}}</td>
                    <td>{{$client['address'] ? $client['address'] : '-'}}</td>
                    <td>{{$client['nationality'] ? $client['nationality'] : '-'}}</td>
                    <td>{{$client['dob'] ? $client['dob'] : '-'}}</td>
                    <td>{{array_key_exists('education_background',$client) && $client['education_background'] ? $client['education_background'] : '-'}}</td>
                    <td>{{array_key_exists('preferred_moc',$client) ? ucfirst($client['preferred_moc']) :''}}</td>
                    <td>
                        <div class="btn-group btn-group-xs" role="group">
                            <a href="#" class="btn btn-xs">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true" style="color:#0000ff;"></span>
                            </a>
                            <a href="#" class="btn btn-xs">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true" style="color:#ff0000;"></span>
                            </a>
                        </div>
                    </td>
                </tr>
                @endif
            @endforeach
        @else
            <tr class="danger"><td colspan="100%" class="text-center"> No client </td></tr>
        @endif
        </tbody>
    </table>
    {!! $clients->links() !!}


    @include('client.import.clientModal')
@endsection