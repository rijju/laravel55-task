@extends('layouts.app')
@section('title',$title)
@section('content')
    <h4> {{$title .' - '. $client->name}} </h4>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <!-- Left Section -->
                <div class="col-md-6 col-sm-6">
                    <div class="form-group}}">
                        {!! Form::label('name','Name',['class'=>'control-label']) !!}
                        <p>{{$client->name}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('gender','Gender',['class'=>'control-label']) !!}
                        <p>{{$client->gender}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone','Phone',['class'=>'control-label']) !!}
                        <p>{{$client->phone}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email','Email',['class'=>'control-label']) !!}
                        <p>{{$client->email}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('address','Address',['class'=>'control-label']) !!}
                        <p>{{$client->address}}</p>
                    </div>
                </div>
                <!-- Right Section -->
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        {!! Form::label('nationality','Nationality',['class'=>'control-label']) !!}
                        <p>{{$client->nationality}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('dob','Date of birth',['class'=>'control-label']) !!}
                        <p>{{$client->dob ? $client->dob->format('d/m/Y') : ''}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('education_background','Education Background',['class'=>'control-label']) !!}
                        <p>{{$client->edu_bg}}</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('preferred_moc','Preferred Mode of Contact',['class'=>'control-label']) !!}
                        <p>{{$client->pr_moc}}</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>

@endsection
