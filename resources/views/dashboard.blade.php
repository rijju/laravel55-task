@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center">
        <h1>Welcome to Laravel 5.5!</h1>
        <p>( This is a small CRUD-task for the client section )</p>
        <p>
            <a class="btn btn-primary btn-lg" href="{{url('client/create')}}" role="button">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                Add / View Client
            </a>
        </p>
    </div>
@endsection
