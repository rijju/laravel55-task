<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::resource('client','ClientController');
Route::get('client/download/{format}','ClientController@downloadClients');

Route::get('client/import/{filename}','ClientController@getImportedClients');
Route::post('client/import/{format}','ClientController@importClients');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
